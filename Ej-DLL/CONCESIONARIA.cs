﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ej_DLL
{
    public class CONCESIONARIA
    {
        List<CLIENTE> clientes = new List<CLIENTE>();
        List<AUTO> autos = new List<AUTO>();
        List<VENTA> ventas = new List<VENTA>();

        public void cargarDatos(List<CLIENTE> clientes, List<AUTO> autos, List<VENTA> ventas)
        {
            this.clientes = clientes;
            this.autos = autos;
            this.ventas = ventas;
        }
    }
}