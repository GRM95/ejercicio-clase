﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Ej_DLL
{
	public class AUTO
	{
		public AUTO(string patente, string marca, string modelo, int año, int precio)
		{
			this.patente = patente;
			this.marca = marca;
			this.modelo = modelo;
			this.año = año;
			this.precio = precio;
		}

		private string patente;

		public string Patente
		{
			get { return patente; }
			set { patente = value; }
		}

		private string marca;

		public string Marca
		{
			get { return marca; }
			set { marca = value; }
		}

		private string modelo;

		public string Modelo
		{
			get { return modelo; }
			set { modelo = value; }
		}

		private int precio;

		public int Precio
		{
			get { return precio; }
			set { precio = value; }
		}

		private int año;

		public int Año
		{
			get { return año; }
			set { año = value; }
		}

		public static List<AUTO> listarAutos()
		{
			List<AUTO> autos = new List<AUTO>();
			ACCESO acceso = new ACCESO();

			acceso.Abrir();
			DataTable tablaTmp = acceso.Leer("LISTAR_AUTOS");
			acceso.Cerrar();

			foreach (DataRow registro in tablaTmp.Rows)
			{
				AUTO auto = new AUTO(registro["patente"].ToString(), registro["marca"].ToString(), registro["modelo"].ToString(),
					int.Parse(registro["año"].ToString()), int.Parse(registro["precio"].ToString()));
				autos.Add(auto);
			}

			return autos;
		}

		public static int crearAuto(string patente, string marca, string modelo, int año, int precio)
		{
			ACCESO acceso = new ACCESO();
			List<SqlParameter> parametros = new List<SqlParameter>();

			parametros.Add(acceso.CrearParametro("@patente", patente));
			parametros.Add(acceso.CrearParametro("@año", año));
			parametros.Add(acceso.CrearParametro("@marca", marca));
			parametros.Add(acceso.CrearParametro("@precio", precio));
			parametros.Add(acceso.CrearParametro("@modelo", modelo));

			acceso.Abrir();
			int output = acceso.Escribir("CREAR_AUTO", parametros);
			acceso.Cerrar();

			return output;
		}
		public static int editarAuto(string patente, string marca, string modelo, int año, int precio)
		{
			ACCESO acceso = new ACCESO();
			List<SqlParameter> parametros = new List<SqlParameter>();

			parametros.Add(acceso.CrearParametro("@patente", patente));
			parametros.Add(acceso.CrearParametro("@año", año));
			parametros.Add(acceso.CrearParametro("@marca", marca));
			parametros.Add(acceso.CrearParametro("@precio", precio));
			parametros.Add(acceso.CrearParametro("@modelo", modelo));

			acceso.Abrir();
			int output = acceso.Escribir("MODIFICAR_AUTO", parametros);
			acceso.Cerrar();

			return output;
		}
	}
}