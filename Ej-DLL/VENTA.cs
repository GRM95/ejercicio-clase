﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ej_DLL;

namespace Ej_DLL
{
	public class VENTA
	{
		public VENTA(int dni, string patente, DateTime fecha)
		{
			this.dni = dni;
			this.patente = patente;
			this.fecha = fecha;
		}

		private int dni;

		public int Dni
		{
			get { return dni; }
			set { dni = value; }
		}

		private string patente;

		public string Patente
		{
			get { return patente; }
			set { patente = value; }
		}

		private DateTime fecha;

		public DateTime Fecha
		{
			get { return fecha; }
			set { fecha = value; }
		}

		public static List<VENTA> listarVentas()
		{
			List<VENTA> ventas = new List<VENTA>();
			ACCESO acceso = new ACCESO();

			acceso.Abrir();
			DataTable tablaTmp = acceso.Leer("LISTAR_VENTAS");
			acceso.Cerrar();

			foreach (DataRow registro in tablaTmp.Rows)
			{
				VENTA venta = new VENTA(int.Parse(registro["dni"].ToString()), registro["patente"].ToString(), DateTime.Parse(registro["fecha"].ToString()));
				ventas.Add(venta);
			}

			return ventas;
		}

		public static int crearVenta(int dni, string patente)
		{
			ACCESO acceso = new ACCESO();
			List<SqlParameter> parametros = new List<SqlParameter>();

			parametros.Add(acceso.CrearParametro("@patente", patente));
			parametros.Add(acceso.CrearParametro("@dni", dni));
			parametros.Add(acceso.CrearParametro("@fecha", DateTime.Now));

			acceso.Abrir();
			int output = acceso.Escribir("CREAR_VENTA", parametros);
			acceso.Cerrar();

			return output;
		}

	}
}