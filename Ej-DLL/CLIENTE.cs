﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Ej_DLL
{
	public class CLIENTE
	{
		public CLIENTE(int dni, string nombre, string apellido)
		{
			this.dni = dni;
			this.nombre = nombre;
			this.apellido = apellido;
		}

		private int dni;

		public int Dni
		{
			get { return dni; }
			set { dni = value; }
		}


		private string nombre;

		public string Nombre
		{
			get { return nombre; }
			set { nombre = value; }
		}

		private string apellido;

		public string Apellido
		{
			get { return apellido; }
			set { apellido = value; }
		}

		public static List<CLIENTE> ListarClientes()
		{
			List<CLIENTE> clientes = new List<CLIENTE>();
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			DataTable tablaTmp = acceso.Leer("LISTAR_CLIENTES");
			acceso.Cerrar();

			foreach (DataRow registro in tablaTmp.Rows)
			{
				CLIENTE cliente = new CLIENTE(int.Parse(registro["DNI"].ToString()), registro["nombre"].ToString(), registro["apellido"].ToString());
				clientes.Add(cliente);
			}

			return clientes;
		}

		public static int crearCliente(int dni, string nombre, string apellido)
		{
			ACCESO acceso = new ACCESO();
			List<SqlParameter> parametros = new List<SqlParameter>();

			parametros.Add(acceso.CrearParametro("@dni", dni));
			parametros.Add(acceso.CrearParametro("@nombre", nombre));
			parametros.Add(acceso.CrearParametro("@apellido", apellido));

			acceso.Abrir();
			int output = acceso.Escribir("CREAR_CLIENTE", parametros);
			acceso.Cerrar();

			return output;
		}

		public static int editarCliente(int dni, string nombre, string apellido)
		{
			ACCESO acceso = new ACCESO();
			List<SqlParameter> parametros = new List<SqlParameter>();

			parametros.Add(acceso.CrearParametro("@dni", dni));
			parametros.Add(acceso.CrearParametro("@nombre", nombre));
			parametros.Add(acceso.CrearParametro("@apellido", apellido));

			acceso.Abrir();
			int output = acceso.Escribir("MODIFICAR_PERSONA", parametros);
			acceso.Cerrar();

			return output;
		}


	}
}