﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Ej_DLL;


namespace Ej_Concesionaria
{
    public partial class Form1 : Form
    {

        CONCESIONARIA consecionaria = new CONCESIONARIA();
        ACCESO acceso = new ACCESO();
        bool editandoCliente = false;
        bool editandoAuto = false;

        public void Enlazar()
        {
            DataGridClientes.DataSource = null;
            DataGridClientes.DataSource = CLIENTE.ListarClientes();

            DataGridAutos.DataSource = null;
            DataGridAutos.DataSource = AUTO.listarAutos();

            dataGridVentas.DataSource = null;
            dataGridVentas.DataSource = VENTA.listarVentas();
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCrearAuto_Click_1(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtPatente.Text) && !string.IsNullOrWhiteSpace(txtMarca.Text)
                && !string.IsNullOrWhiteSpace(txtModelo.Text) && !string.IsNullOrWhiteSpace(txtAño.Text)
                && !string.IsNullOrWhiteSpace(txtPrecio.Text))
            {
                if (!editandoAuto)
                {
                    AUTO.crearAuto(txtPatente.Text, txtMarca.Text, txtModelo.Text, int.Parse(txtAño.Text), int.Parse(txtPrecio.Text));
                }
                else
                {
                    AUTO.editarAuto(txtPatente.Text, txtMarca.Text, txtModelo.Text, int.Parse(txtAño.Text), int.Parse(txtPrecio.Text));
                    editandoAuto = false;
                    txtPatente.Enabled = true;
                    btnCrearAuto.Text = "CREAR";
                }

                Enlazar();
                txtPatente.Clear();
                txtMarca.Clear();
                txtModelo.Clear();
                txtPrecio.Clear();
                txtAño.Clear();
            }
            else
            {
                MessageBox.Show("Complete todos los campos.");
            }
        }

        private void btnCrearCliente_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDni.Text) && !string.IsNullOrWhiteSpace(txtNombre.Text)
                && !string.IsNullOrWhiteSpace(txtApellido.Text))
            {
                if (!editandoCliente)
                {
                    CLIENTE.crearCliente(int.Parse(txtDni.Text), txtNombre.Text, txtApellido.Text);
                }
                else
                {
                    CLIENTE.editarCliente(int.Parse(txtDni.Text), txtNombre.Text, txtApellido.Text);
                    editandoCliente = false;
                    txtDni.Enabled = true;
                    btnCrearCliente.Text = "CREAR";
                }

                Enlazar();
                txtDni.Clear();
                txtNombre.Clear();
                txtApellido.Clear();
            }
            else
            {
                MessageBox.Show("Complete todos los campos.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnVender_Click_1(object sender, EventArgs e)
        {
            if (DataGridAutos.SelectedRows.Count > 0)
            {
                if (DataGridClientes.SelectedRows.Count > 0)
                {
                    VENTA.crearVenta(((CLIENTE)DataGridClientes.SelectedRows[0].DataBoundItem).Dni, ((AUTO)DataGridAutos.SelectedRows[0].DataBoundItem).Patente);
                    Enlazar();
                }
                else
                {
                    MessageBox.Show("Seleccione un cliente");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un auto");
            }
        }

    }
}
